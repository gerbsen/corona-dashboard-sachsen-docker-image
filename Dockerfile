FROM r-base:3.6.3

RUN apt-get update && apt-get -y install software-properties-common
RUN add-apt-repository 'deb https://deb.nodesource.com/node_12.x buster main'
RUN apt-get install -y nodejs npm postgresql-client libpq-dev curl

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
